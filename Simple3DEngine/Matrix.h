#pragma once

struct Matrix4x4
{
	float M[4][4]{ 0 };
};