#pragma once
#include <vector>
#include <string>
#include <fstream>
#include <strstream>
#include "Triangle.h"

using std::vector;
using std::string;
using std::ifstream;
using std::strstream;

struct Mesh
{
	vector<Triangle> Tris;

	bool LoadFromObjectFile(string sFileName)
	{
		ifstream Is{ sFileName };
		if (!Is)
			return false;

		vector<Vector3D> Vertices;

		while (!Is.eof())
		{
			char Line[256];
			Is.getline(Line, 256);

			strstream Ss;
			Ss << Line;

			char Junk;

			if (Line[0] == 'v')
			{
				Vector3D V;
				Ss >> Junk >> V.X >> V.Y >> V.Z;
				Vertices.push_back(V);
			}

			if (Line[0] == 'f')
			{
				int F[3];
				Ss >> Junk >> F[0] >> F[1] >> F[2];
				Tris.push_back({ Vertices[F[0] - 1], Vertices[F[1] - 1], Vertices[F[2] - 1] });
			}
		}

		Is.close();
		return true;
	}
};
